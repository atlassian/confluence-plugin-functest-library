package com.atlassian.confluence.plugin.functest.module.xmlrpc.mail;

import com.atlassian.confluence.rpc.RemoteException;
import com.atlassian.confluence.rpc.SecureRpc;

import java.util.Hashtable;

public interface MailServerHelperService extends SecureRpc
{

    String createMailServer(final String authToken, final Hashtable mailServerStructure) throws RemoteException;

    boolean updateMailServer(final String authToken, final Hashtable mailServerStructure) throws RemoteException;

    Hashtable<String, String> readMailServer(final String authToken, final String mailServerId) throws RemoteException;

    boolean deleteMailServer(final String authToken, final String mailServerId) throws RemoteException;

    Hashtable<String, String> getMailServerIdsAndNames(final String authToken) throws RemoteException;
}
