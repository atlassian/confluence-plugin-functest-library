package com.atlassian.confluence.plugin.functest.util;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import java.util.Properties;
import java.io.InputStream;
import java.io.IOException;

public class ConfluenceBuildUtil
{
    private static final Logger LOG = Logger.getLogger(ConfluenceBuildUtil.class);

    public static Properties getBuildProperties() throws IOException
    {
        Properties buildProperties = new Properties();
        InputStream in = null;
        try
        {
            in = ConfluenceBuildUtil.class.getClassLoader().getResourceAsStream("com/atlassian/confluence/default.properties");
            buildProperties.load(in);

            return buildProperties;
        }
        finally
        {
            IOUtils.closeQuietly(in);
        }
    }

    public static int getBuildNumber()
    {
        try
        {
            return Integer.parseInt(getBuildProperties().getProperty("build.number"));
        }
        catch (IOException ioe)
        {
            LOG.error("Unable to read default.properties", ioe);
            return -1;
        }
    }

    public static boolean isOnDemandMode() throws IOException
    {
        Boolean isOnDemandMode = false;
        Properties properties = getBuildProperties();
        String buildVersion = (String) properties.getOrDefault("version.number", "");
        if (buildVersion.startsWith("100"))
        {
            isOnDemandMode = true;
        }

        return isOnDemandMode;
    }

    public static boolean containsSudoFeature()
    {
        return getBuildNumber() >= 1904;
    }
}
