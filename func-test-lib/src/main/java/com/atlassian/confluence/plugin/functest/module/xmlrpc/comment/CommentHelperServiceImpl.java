package com.atlassian.confluence.plugin.functest.module.xmlrpc.comment;


import com.atlassian.confluence.rpc.RemoteException;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

public class CommentHelperServiceImpl implements CommentHelperService
{
    private CommentHelperService commentHelperServiceDelegate;

    private PlatformTransactionManager transactionManager;

    @SuppressWarnings("unused")
    public void setCommentHelperServiceDelegate(CommentHelperService commentHelperServiceDelegate)
    {
        this.commentHelperServiceDelegate = commentHelperServiceDelegate;
    }

    @SuppressWarnings("unused")
    public void setTransactionManager(PlatformTransactionManager transactionManager)
    {
        this.transactionManager = transactionManager;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Map<String, ?> getComment(final String authenticationToken, final String commentId)
    {
        return (Map<String, ?>) new TransactionTemplate(transactionManager).execute(
                (TransactionCallback) transactionStatus -> commentHelperServiceDelegate.getComment(authenticationToken, commentId)
        );
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<String> getCommentIds(final String authenticationToken, final String pageId)
    {
        return (List<String>) new TransactionTemplate(transactionManager).execute(
                (TransactionCallback) transactionStatus -> commentHelperServiceDelegate.getCommentIds(authenticationToken, pageId)
        );
    }

    @Override
    public String addOrUpdateComment(final String authenticationToken, final Hashtable<String, ?> commentStruct)
    {

        return (String) new TransactionTemplate(transactionManager).execute(
                (TransactionCallback) transactionStatus -> commentHelperServiceDelegate.addOrUpdateComment(authenticationToken, commentStruct)
        );
    }

    @Override
    public String login(String s, String s1) throws RemoteException
    {
        return null;
    }

    @Override
    public boolean logout(String s) throws RemoteException
    {
        return false;
    }
}
