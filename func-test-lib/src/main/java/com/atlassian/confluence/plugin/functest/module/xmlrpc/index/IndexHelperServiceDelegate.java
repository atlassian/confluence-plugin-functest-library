package com.atlassian.confluence.plugin.functest.module.xmlrpc.index;

import com.atlassian.confluence.rpc.RemoteException;
import com.atlassian.confluence.search.ReIndexTask;
import com.atlassian.confluence.search.lucene.ConfluenceIndexManager;
import com.atlassian.spring.container.ContainerManager;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.concurrent.TimeUnit;


public class IndexHelperServiceDelegate implements IndexHelperService
{
    private static final Logger logger = Logger.getLogger(IndexHelperServiceDelegate.class);

    public Boolean reindex(final String authenticationToken)
    {
        try
        {
            ReIndexTask task = getIndexManager().reIndex();
            while (!task.isFinishedReindexing())
                TimeUnit.MILLISECONDS.sleep(500);

            return Boolean.TRUE;
        }
        catch (IOException e)
        {
            logger.error("Unable to reindex Confluence.", e);
            return Boolean.FALSE;
        } catch (InterruptedException e) {
            logger.error("Reindex is interrupted", e);
            Thread.interrupted();
            return Boolean.FALSE;
        }
    }

    public Boolean flush(final String authenticationToken) {
        try
        {
            getIndexManager().flushQueue();

            return Boolean.TRUE;
        }
        catch (IOException ioe)
        {
            logger.error("Unable to flush index queue.", ioe);
            return Boolean.FALSE;
        }
    }

    private ConfluenceIndexManager getIndexManager() throws IOException
    {
        return (ConfluenceIndexManager) ContainerManager.getComponent("indexManager");
    }

    public String login(String s, String s1) throws RemoteException
    {
        return null;
    }

    public boolean logout(String s) throws RemoteException
    {
        return false;
    }
}
