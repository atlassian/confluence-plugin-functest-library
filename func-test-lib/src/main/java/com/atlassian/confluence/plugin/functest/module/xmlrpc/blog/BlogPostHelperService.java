package com.atlassian.confluence.plugin.functest.module.xmlrpc.blog;

import com.atlassian.confluence.rpc.RemoteException;
import com.atlassian.confluence.rpc.SecureRpc;

import java.util.Date;
import java.util.Map;

public interface BlogPostHelperService extends SecureRpc {

    String getBlogPostId(final String authenticationToken, final String spaceKey, final String title, final Date day)
            throws RemoteException;

    Map<String, ? > getBlogPost(final String authenticationToken, final String id);
}
