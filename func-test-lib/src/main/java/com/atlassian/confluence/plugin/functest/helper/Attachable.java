package com.atlassian.confluence.plugin.functest.helper;

public interface Attachable {

    String[] getAttachmentFileNames();
}
