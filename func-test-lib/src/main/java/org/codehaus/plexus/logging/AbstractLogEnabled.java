package org.codehaus.plexus.logging;

import com.atlassian.annotations.Internal;

/**
 * The plexus-sec-dispatcher module depends on this class, which comes from another module that it doesn't
 * declare a dependency on. We stub it out here.
 * We don't want to bring in the actual plexus logging stuff, as it strings in a *lot* of crap we don't want, like
 * sisu-guice.
 */
@Internal
public class AbstractLogEnabled
{
}
