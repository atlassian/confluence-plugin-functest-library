package it.com.atlassian.confluence.plugin.functest;

import com.atlassian.confluence.plugin.functest.AbstractConfluencePluginWebTestCase;
import com.atlassian.confluence.plugin.functest.util.ConfluenceBuildUtil;

public class ConfluenceWebTesterTestCase extends AbstractConfluencePluginWebTestCase
{
    public void testAssertUrlProtectedFromXsrfExploits()
    {
        if (ConfluenceBuildUtil.getBuildNumber() < 1615 || ConfluenceBuildUtil.containsSudoFeature())
            return;

        assertResourceXsrfProtected("/admin/plugins.action");
    }
}
