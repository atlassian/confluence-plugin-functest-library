package it.com.atlassian.confluence.plugin.functest;

import com.atlassian.confluence.plugin.functest.AbstractConfluencePluginWebTestCase;
import com.atlassian.confluence.plugin.functest.helper.BlogPostHelper;
import com.atlassian.confluence.plugin.functest.helper.AttachmentHelper;
import org.apache.commons.lang.time.FastDateFormat;
import org.xml.sax.SAXException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.io.IOException;

public class BlogPostHelperTestCase extends AbstractConfluencePluginWebTestCase
{

    private boolean createBlogPost(final BlogPostHelper blogPostHelper, final String spaceKey, final String title, final String content) {
        blogPostHelper.setTitle(title);
        blogPostHelper.setSpaceKey(spaceKey);
        blogPostHelper.setContent(content);
        blogPostHelper.setCreationDate(new Date());

        return blogPostHelper.create();
    }

    public void testCreateBlog() {
        final BlogPostHelper blogPostHelper = getBlogPostHelper();

        assertTrue(createBlogPost(blogPostHelper, "ds", "Test Blog", "This is a test blog post"));
        assertFalse(0 == blogPostHelper.getId());

        gotoPage("/display/" + blogPostHelper.getSpaceKey() + FastDateFormat.getInstance("/yyyy/MM/dd").format(blogPostHelper.getCreationDate()) + "/Test+Blog");
        assertTextPresent("This is a test blog post");

        assertTrue(blogPostHelper.delete());
    }

    public void testEditBlog() {
        final BlogPostHelper blogPostHelper = getBlogPostHelper();
        final long createdBlogPostId;

        assertTrue(createBlogPost(blogPostHelper, "ds", "Test Blog", "This is a test blog post to be edited"));
        assertFalse(0 == (createdBlogPostId = blogPostHelper.getId()));

        gotoPage("/display/" + blogPostHelper.getSpaceKey() + FastDateFormat.getInstance("/yyyy/MM/dd").format(blogPostHelper.getCreationDate()) + "/Test+Blog");
        assertTextPresent("This is a test blog post to be edited");

        blogPostHelper.setTitle("Test Edited Blog");
        blogPostHelper.setContent("This is an edited test blog.");

        assertTrue(blogPostHelper.update());

        /* Ensure we did not create a new page */
        assertEquals(createdBlogPostId, blogPostHelper.getId());

        gotoPage("/display/" + blogPostHelper.getSpaceKey() + FastDateFormat.getInstance("/yyyy/MM/dd").format(blogPostHelper.getCreationDate()) + "/Test+Edited+Blog");
        assertTextPresent("This is an edited test blog.");

        assertTrue(blogPostHelper.delete());
    }

    public void testDeleteBlog() {
        final BlogPostHelper blogPostHelper = getBlogPostHelper();

        assertTrue(createBlogPost(blogPostHelper, "ds", "Test Delete Blog", "This is a page to be removed."));
        assertFalse(0 == blogPostHelper.getId());

        gotoPage("/display/" + blogPostHelper.getSpaceKey() + FastDateFormat.getInstance("/yyyy/MM/dd").format(blogPostHelper.getCreationDate()) + "/Test+Delete+Blog");
        assertTextPresent("This is a page to be removed.");

        assertTrue(blogPostHelper.delete());
    }

    public void testReadBlogPost() throws ParseException, SAXException {
        final BlogPostHelper blogPostHelper = getBlogPostHelper();
        final long octagonBlogPostId;

        blogPostHelper.setSpaceKey("ds");
        blogPostHelper.setCreationDate(new SimpleDateFormat("ddMMyyyy").parse("21112004"));
        blogPostHelper.setTitle("Octagon blog post");

        octagonBlogPostId = blogPostHelper.findBySpaceKeyPublishedDateAndBlogPostTitle();
        assertTrue(octagonBlogPostId > 0);

        blogPostHelper.setId(octagonBlogPostId);
        assertTrue(blogPostHelper.read());

        assertEquals("ds", blogPostHelper.getSpaceKey());
        assertEquals("Octagon blog post", blogPostHelper.getTitle());
        assertTrue(blogPostHelper.getVersion() > 0);

        assertTrue(blogPostHelper.getContent().indexOf("Here is a blog with a guest appearance of the word 'octagon'") >= 0);
    }

    public void testAddLabelToBlog() throws ParseException {
        final BlogPostHelper blogPostHelper = getBlogPostHelper();
        final long octagonBlogPostId;
        final List labels;
        final BlogPostHelper anotherBlogPostHelper;

        blogPostHelper.setSpaceKey("ds");
        blogPostHelper.setCreationDate(new SimpleDateFormat("ddMMyyyy").parse("21112004"));
        blogPostHelper.setTitle("Octagon blog post");

        octagonBlogPostId = blogPostHelper.findBySpaceKeyPublishedDateAndBlogPostTitle();
        assertTrue(octagonBlogPostId > 0);

        blogPostHelper.setId(octagonBlogPostId);
        assertTrue(blogPostHelper.read());

        blogPostHelper.setLabels(Arrays.asList("my:label1", "team:label2", "label3"));
        assertTrue(blogPostHelper.update());

        anotherBlogPostHelper = getBlogPostHelper(blogPostHelper.getId());
        anotherBlogPostHelper.read();

        labels = anotherBlogPostHelper.getLabels();
        assertTrue(labels.containsAll(Arrays.asList("my:label1", "team:label2", "label3")));
    }

    public void testReduceLabelsOnBlog() throws ParseException {
        final BlogPostHelper blogPostHelper = getBlogPostHelper();
        final long octagonBlogPostId;
        final List labels;
        final BlogPostHelper anotherBlogPostHelper;
        final BlogPostHelper yetAnotherBlogPostHelper;

        blogPostHelper.setSpaceKey("ds");
        blogPostHelper.setCreationDate(new SimpleDateFormat("ddMMyyyy").parse("21112004"));
        blogPostHelper.setTitle("Octagon blog post");

        octagonBlogPostId = blogPostHelper.findBySpaceKeyPublishedDateAndBlogPostTitle();
        assertTrue(octagonBlogPostId > 0);

        blogPostHelper.setId(octagonBlogPostId);
        assertTrue(blogPostHelper.read());

        blogPostHelper.setLabels(Arrays.asList("my:label1", "team:label2"));
        assertTrue(blogPostHelper.update());

        anotherBlogPostHelper = getBlogPostHelper(blogPostHelper.getId());
        anotherBlogPostHelper.read();

        labels = anotherBlogPostHelper.getLabels();
        assertTrue(labels.containsAll(Arrays.asList("my:label1", "team:label2")));


        anotherBlogPostHelper.setLabels(Arrays.asList("my:label1"));
        assertTrue(anotherBlogPostHelper.update());

        yetAnotherBlogPostHelper = getBlogPostHelper(blogPostHelper.getId());
        assertTrue(yetAnotherBlogPostHelper.read());

        assertEquals(1, yetAnotherBlogPostHelper.getLabels().size());
        assertTrue(yetAnotherBlogPostHelper.getLabels().containsAll(Arrays.asList("my:label1")));
    }

    @SuppressWarnings({ "deprecation" })
    public void testGetBlogContentRendered() {
        final BlogPostHelper blogPostHelper = getBlogPostHelper();

        blogPostHelper.setSpaceKey("ds");
        blogPostHelper.setTitle("Test Blog");
        blogPostHelper.setContent("Click [here|DS:Confluence Overview] to view the home page of the DS space");
        blogPostHelper.setCreationDate(new Date());

        assertTrue(blogPostHelper.create());
        assertTrue(blogPostHelper.getContentRendered().indexOf(">here</a") >= 0);
        assertTrue(blogPostHelper.delete());
    }

    public void testGetAttachmentNames() throws IOException {
        final String[] attachmentNames = new String[]  { "file1.txt", "file2.txt" };
        final BlogPostHelper blogPostHelper = getBlogPostHelper();
        final List actualAttachmentNames;

        blogPostHelper.setSpaceKey("ds");
        blogPostHelper.setTitle("Test Blog");
        blogPostHelper.setContent("Click [here|DS:Confluence Overview] to view the home page of the DS space");
        blogPostHelper.setCreationDate(new Date());

        assertTrue(blogPostHelper.create());

        for (String attachmentName : attachmentNames)
        {
            final AttachmentHelper attachmentHelper = getAttachmentHelper();
            final byte[] data = attachmentName.getBytes("UTF-8");

            attachmentHelper.setParentId(blogPostHelper.getId());
            attachmentHelper.setFileName(attachmentName);
            attachmentHelper.setContentType("text/plain");
            attachmentHelper.setContentLength(data.length);
            attachmentHelper.setContent(data);

            assertTrue(attachmentHelper.create());
        }

        actualAttachmentNames = Arrays.asList(blogPostHelper.getAttachmentFileNames());

        assertEquals(2, actualAttachmentNames.size());
        assertTrue(Arrays.asList(attachmentNames).containsAll(actualAttachmentNames));
        assertTrue(blogPostHelper.delete());
    }
}
